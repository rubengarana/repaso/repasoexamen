 Declaración e inicialización de un array de enteros de tamaño 10
- Introducción de valores en el array y visualización de los mismos: en cada posición el contenido será su número de posición
- Modificación del array y visualización del mismo: en las posiciones pares del array se modificará el contenido que tenga por 0
- Visualización del número de ceros que hay en el array.
- Visualización del número de elementos distintos de cero que hay en el array.